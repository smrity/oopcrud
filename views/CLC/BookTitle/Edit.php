
<?php
require_once ("../../../vendor/autoload.php");
$objBookTitle=new\App\BookTitle\BookTitle;
$objBookTitle->setdata($_GET);
$oneData=$objBookTitle->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta charset="UTF-8">
    <link style="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link style="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="navbar">
        <td> <a href="index.php" class="btn btn-group-1g btn-info">Active-List</a> </td>

    </div>
    <form class="form-group f" action="update.php" method="post">
        Enter Book Name:
        <input class="form-control" type="text" name="bookName" value="<?php echo $oneData->bookName?>">
        <br>
        Enter Author Name:
        <input class="form-control" type="text" name="authorName"value="<?php echo $oneData->authorName?>">
        <br>
        <input type="hidden" name="id"value="<?php echo $oneData->id?>">
        <input type="submit">

    </form>
</div>

</body>
</html>